package cn.ecar.tumc.cort;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.net.URLDecoder;
import java.util.Properties;

public class Comment {
	
	static Properties properties = null;
	
	static {
		
		try {
			String filePath = URLDecoder.decode(Comment.class.getProtectionDomain().getCodeSource().getLocation().getPath(), "UTF-8");
			File f = new File(filePath);
			if (f.isFile()) {
				filePath = f.getParent()+File.separator;
			}
			properties = new Properties();
			properties.load(new InputStreamReader(new FileInputStream(filePath+"config.properties"),"UTF-8"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("error!!! not find config.properties");
			e.printStackTrace();
			System.exit(0);
		}
	}
	
	protected static String PROJECT_PATH = properties.getProperty("project.path");
	protected static String ENTITY_PACKAGE_NAME = properties.getProperty("entity.package.name");
	protected static String CLASS_NAME = properties.getProperty("class.name");	
	protected static String TABLENAME = properties.getProperty("table.name").toUpperCase(); 
	protected static String DRIVER = "com.mysql.jdbc.Driver";   
	protected static String URLSTR = properties.getProperty("db.mysql.url");  
	protected static String USERNAME = properties.getProperty("db.mysql.username");  
	protected static String USERPASSWORD = properties.getProperty("db.mysql.password");  
	
	public enum ENUM_BASEDAO{
		list,insert,find,update,delete,insertBatch,updateBatch,deleteBatch
	}
	
}
